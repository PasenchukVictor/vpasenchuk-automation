package com.thumbtack.selenium_kt

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import org.apache.commons.io.FileUtils

import java.io.File
import java.io.IOException

/**
 * Created by pasencukviktor on 15/12/2016
 */
class SiteCredentials(

        @SerializedName("BASE_URL")
        val baseUrl: String,

        @SerializedName("LOGIN")
        val login: String,

        @SerializedName("PASSWORD")
        val password: String) {

    companion object {
        @Throws(IOException::class)
        fun getInstanceFromFile(fileName: String): SiteCredentials {
            val s = FileUtils.readFileToString(File(fileName))
            return Gson().fromJson(s, SiteCredentials::class.java)
        }
    }
}
