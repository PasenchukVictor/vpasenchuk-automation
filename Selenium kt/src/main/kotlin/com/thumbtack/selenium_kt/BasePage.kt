package com.thumbtack.selenium_kt

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.PageFactory

import java.util.concurrent.TimeUnit

/**
 * Created by pasencukviktor on 03/12/2016
 */
abstract class BasePage(val driver: WebDriver) {

    init {
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT_TIME.toLong(), TimeUnit.SECONDS)

        PageFactory.initElements(driver, this)
    }

    constructor(page: BasePage) : this(page.driver)

    @JvmOverloads fun sleep(ms: Long = DEFAULT_SLEEP_TIME) {
        try {
            Thread.sleep(ms)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    fun isElementPresent(locator: By): Boolean {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS)
        val b = driver.findElements(locator).size > 0
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT_TIME.toLong(), TimeUnit.SECONDS)
        return b
    }

    fun isChildElementPresent(element: WebElement, locator: By): Boolean {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS)
        val b = element.findElements(locator).size > 0
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT_TIME.toLong(), TimeUnit.SECONDS)
        return b
    }

    companion object {
        val DEFAULT_SLEEP_TIME = 5000L
        val IMPLICIT_WAIT_TIME = 15
    }

}
