package com.thumbtack.selenium_kt.redmine

import com.thumbtack.selenium_kt.BasePage
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy

/**
 * Created by pasencukviktor on 03/12/2016
 */
open class RedminePage : BasePage {

    @FindBy(xpath = "//a[@class='home']")
    private lateinit var homeButton: WebElement

    constructor(webDriver: WebDriver) : super(webDriver)

    constructor(page: BasePage) : super(page)

    val isLoggedIn: Boolean
        get() = !isElementPresent(LOGIN_LOCATOR)

    fun clickSignInButton() {
        driver.findElement(LOGIN_LOCATOR).click()
    }

    fun clickSignOutButton() {
        driver.findElement(LOGOUT_LOCATOR).click()
    }

    fun clickProfileButton(): ProfilePage {
        driver.findElement(PROFILE_LOCATOR).click()
        return ProfilePage(driver)
    }

    fun clickTimelogButton(): TimelogPage {
        driver.findElement(TIMELOG_LOCATOR).click()

        return TimelogPage(this)
    }

    fun clickHomeButton() {
        homeButton.click()
    }

    companion object {

        private val LOGIN_LOCATOR = By.xpath("//li[1]/a[@class='login']")
        private val LOGOUT_LOCATOR = By.xpath("//li[2]/a[@class='logout']")
        private val TIMELOG_LOCATOR = By.xpath("//li[4]")
        private val PROFILE_LOCATOR = By.xpath("//li[1]/a[@class='my-account']")
    }

}
