package com.thumbtack.selenium_kt.redmine

import com.thumbtack.selenium_kt.BasePage
import com.thumbtack.selenium_kt.redmine.models.NotificationType
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.ui.Select

/**
 * Created by pasencukviktor on 03/12/2016
 */
class ProfilePage : RedminePage {

    @FindBy(xpath = "//input[@id='no_self_notified']")
    private lateinit var noSelfNotifiedCheckbox: WebElement

    @FindBy(xpath = "//select[@id='user_mail_notification']")
    private lateinit var mailNotificationDropdown: WebElement

    @FindBy(xpath = "//div[@class='splitcontentleft']/input")
    private lateinit var saveButton: WebElement


    constructor(webDriver: WebDriver) : super(webDriver)

    constructor(page: BasePage) : super(page)

    fun selectNotificationType(notificationType: NotificationType) {
        Select(mailNotificationDropdown)
                .selectByValue(notificationType.name)
    }

    val selectedNotificationType: NotificationType
        get() = NotificationType.valueOf(Select(mailNotificationDropdown)
                .firstSelectedOption.getAttribute("value"))

    fun setNoSelfNotifiedCheckboxState(checked: Boolean) {
        val noSelfNotificationsSelected = isNoSelfNotificationsSelected
        if (noSelfNotificationsSelected && !checked || !noSelfNotificationsSelected && checked)
            noSelfNotifiedCheckbox.click()
    }

    val isNoSelfNotificationsSelected: Boolean
        get() = noSelfNotifiedCheckbox.isSelected

    fun clickSaveButton() {
        saveButton.click()
    }

}
