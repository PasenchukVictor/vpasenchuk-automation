package com.thumbtack.selenium_kt.redmine

import com.thumbtack.selenium_kt.BasePage
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy

/**
 * Created by pasencukviktor on 15/12/2016
 */
class TimeEntryDialog(page: BasePage) : RedminePage(page) {

    @FindBy(id = "new-time-entry-ok-button")
    internal lateinit var okButton: WebElement

    @FindBy(id = "delete-time-entry")
    internal lateinit var deleteButton: WebElement

    @FindBy(id = "hours")
    internal lateinit var hoursField: WebElement

    fun addHours(hours: Float) {
        hoursField.sendKeys(hours.toString())
    }


    fun pressOkButton() {
        okButton.click()

        switchToactiveElement()
    }

    fun pressDeleteButton() {
        deleteButton.click()

        switchToactiveElement()
    }

    private fun switchToactiveElement() {
        driver.switchTo().activeElement()
    }

    init {
        switchToactiveElement()
    }

}
