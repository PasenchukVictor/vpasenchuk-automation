package com.thumbtack.selenium_kt.redmine.models

/**
 * Created by pasencukviktor on 03/12/2016
 */
enum class NotificationType {
    all,
    only_my_events,
    only_assigned,
    only_owner,
    none
}
