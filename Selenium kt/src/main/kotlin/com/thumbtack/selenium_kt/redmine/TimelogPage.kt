package com.thumbtack.selenium_kt.redmine

import com.thumbtack.selenium_kt.redmine.models.TimelogCell
import org.openqa.selenium.By
import org.openqa.selenium.NoAlertPresentException
import org.openqa.selenium.UnhandledAlertException
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import java.util.*

/**
 * Created by pasencukviktor on 14/12/2016
 */
class TimelogPage(redminePage: RedminePage) : RedminePage(redminePage) {

    val DAILY_SUM_IDS = arrayOf("Mon-sum", "Tue-sum", "Wed-sum", "Thu-sum", "Fri-sum", "Sat-sum", "Sun-sum")
    val ADD_TIME_ENTRY_LOCATOR: By = By.xpath(".//div[@class='new-time-entry icon-plus']")

    private val random = Random()

    @FindBy(xpath = "//table[@class='quicktrack-table']/tbody")
    private lateinit var timelogTable: WebElement

    @FindBy(xpath = "//tbody/tr[@class='summary-row']/td[@class='day-summary-hours summary-hours-total']")
    private lateinit var totalSum: WebElement

    private val freeTimelogCells: MutableList<TimelogCell>
    private val issues: Set<String>

    init {
        sleep()
        val rows = timelogTable.findElements(By.xpath(".//tr[contains (@id, 'project-')]"))

        issues = rows.map { it.getAttribute("id") }.toSet()
        freeTimelogCells = rows
                .flatMap {
                    val rowId = it.getAttribute("id")
                    it.findElements(By.xpath(".//td[contains (@class, 'Days')]"))
                            .filter { isChildElementPresent(it, ADD_TIME_ENTRY_LOCATOR) }
                            .map { TimelogCell(rowId, it.getAttribute("class")) }
                }
                .toMutableList()

//        freeTimelogCells = LinkedList()
//        for (row in rows) {
//            val rowId = row.getAttribute("id")
//            issues.add(rowId)
//            val columns = row.findElements(By.xpath(".//td[contains (@class, 'Days')]"))
//            for (column in columns) {
//                val columnId = column.getAttribute("class")
//
//                if (isChildElementPresent(column, ADD_TIME_ENTRY_LOCATOR))
//                    freeTimelogCells.add(TimelogCell(rowId, columnId))
//            }
//        }
    }

    fun getTotalSum(): Double {
        return totalSum.text.toDouble()
    }

    fun totalSumByDays(): Double =
            DAILY_SUM_IDS.fold(0.0) { sum, id ->
                sum + driver
                        .findElement(By.id(id))
                        .text.toDouble()
            }

//    fun getTotalSumByDays(): Float {
//        var sum = 0f
//        for (dailySumId in DAILY_SUM_IDS) {
//            val dailySum = driver
//                    .findElement(By.id(dailySumId))
//                    .getText()
//            sum += java.lang.Float.parseFloat(dailySum)
//        }
//        return sum
//    }

    fun totalSumByIssues(): Double = issues.fold(0.0) { sum, issue ->
        sum + driver
                .findElement(By.id(issue))
                .findElement(By.className("task-hours-sum"))
                .text.toDouble()
    }

    val randomFreeCell: TimelogCell?
        get() {
            while (freeTimelogCells.size > 0) {
                val i = random.nextInt(freeTimelogCells.size)

                val timelogCell = freeTimelogCells.removeAt(i)

                if (isElementPresent(getTimelogCellButtonXpath(timelogCell)))
                    return timelogCell
            }
            return null
        }

    private fun getTimelogCellButtonXpath(timelogCell: TimelogCell): By {
        return By.xpath("//tbody/tr[@id='${timelogCell.row}']/td[@class='${timelogCell.column}']/div[@class='new-time-entry icon-plus']")
    }

    private fun getTimelogCellEntryXpath(timelogCell: TimelogCell): By {
        return By.xpath("//tbody/tr[@id='${timelogCell.row}']/td[@class='${timelogCell.column}']")
    }

    fun addTimeEntryToCell(timelogCell: TimelogCell, hours: Float): Boolean {

        val timelogCellButtonXpath = getTimelogCellButtonXpath(timelogCell)
        if (isElementPresent(timelogCellButtonXpath)) {
            driver
                    .findElement(timelogCellButtonXpath)
                    .click()

            val timeEntryDialog = TimeEntryDialog(this)
            timeEntryDialog.addHours(hours)
            timeEntryDialog.pressOkButton()

            return true

        }

        return false
    }

    fun deleteTimeEntryFromCell(timelogCell: TimelogCell) {

        val timelogCellButtonXpath = getTimelogCellEntryXpath(timelogCell)
        driver
                .findElement(timelogCellButtonXpath)
                .click()

        val timeEntryDialog = TimeEntryDialog(this)

        try {
            timeEntryDialog.pressDeleteButton()
        } catch (f: UnhandledAlertException) {
            try {
                val alert = driver.switchTo().alert()
                alert.accept()
            } catch (e: NoAlertPresentException) {
                e.printStackTrace()
            }
        }
    }

}
