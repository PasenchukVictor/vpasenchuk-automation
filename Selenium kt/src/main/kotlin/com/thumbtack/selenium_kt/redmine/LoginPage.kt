package com.thumbtack.selenium_kt.redmine

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy

/**
 * Created by pasencukviktor on 03/12/2016
 */
class LoginPage(webDriver: WebDriver) : RedminePage(webDriver) {

    @FindBy(xpath = "//input[@id='username']")
    private lateinit var usernameField: WebElement

    @FindBy(id = "password")
    private lateinit var passwordField: WebElement

    @FindBy(xpath = "//form/table/tbody/tr[4]/td[2]/input")
    private lateinit var loginButton: WebElement

    fun setUsername(username: String) {
        usernameField.sendKeys(username)
    }

    fun setPassword(password: String) {
        passwordField.sendKeys(password)
    }

    fun clickLoginButton() {
        loginButton.click()
    }

}
