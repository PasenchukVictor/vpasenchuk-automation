package com.thumbtack.selenium_kt.redmine.models

/**
 * Created by pasencukviktor on 14/12/2016
 */
class TimelogCell(val row: String?, val column: String?) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false

        val that = other as TimelogCell?

        if (if (row != null) row != that!!.row else that!!.row != null) return false
        return if (column != null) column == that.column else that.column == null

    }

    override fun hashCode(): Int {
        var result = row?.hashCode() ?: 0
        result = 31 * result + (column?.hashCode() ?: 0)
        return result
    }
}
