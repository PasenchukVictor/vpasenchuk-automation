package com.thumbtack.selenium_kt

import com.thumbtack.selenium_kt.redmine.RedminePage
import com.thumbtack.selenium_kt.redmine.TimelogPage
import com.thumbtack.selenium_kt.redmine.models.TimelogCell
import org.testng.Assert
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import java.io.IOException
import java.util.*

/**
 * Created by pasencukviktor on 15/12/2016
 */
class TimeLogTest : BaseTest() {

    private lateinit var timelogPage: TimelogPage

    override val configFile: String
        get() = "real_redmine.json"

    @BeforeClass
    @Throws(IOException::class)
    override fun setupTest() {
        super.setupTest()

        timelogPage = RedminePage(driver).clickTimelogButton()
    }

    @Test
    @Throws(Throwable::class)
    fun timeLogTestRowSum() {

        TestStepConductor(driver, "timeLogTestRowSum")
                .setDefaultFolder(BaseTest.ERROR_SCREENSHOTS_FOLDER)
                .nextStep("Check row values") { Assert.assertEquals(timelogPage.getTotalSum(), timelogPage.totalSumByIssues(), DELTA, "Row sum is incorrect") }

    }

    @Test
    @Throws(Throwable::class)
    fun timeLogTestColumnSum() {
        TestStepConductor(driver, "timeLogTestColumnSum")
                .setDefaultFolder(BaseTest.ERROR_SCREENSHOTS_FOLDER)
                .nextStep("Check column values") { Assert.assertEquals(timelogPage.getTotalSum(), timelogPage.totalSumByDays(), DELTA, "Column sum is incorrect") }

    }

    @Test
    @Throws(Throwable::class)
    fun timeLogTestAddTime() {
        val timelogCells = LinkedList<TimelogCell>()

        var totalSum = 0.0
        TestStepConductor(driver, "timeLogTestColumnSum")
                .setDefaultFolder(BaseTest.ERROR_SCREENSHOTS_FOLDER)
                .nextStep("Add time entries") {

                    totalSum = timelogPage.getTotalSum()

                    var addSum = 0f
                    for (i in 0..2) {
                        val randomFreeCell = timelogPage.randomFreeCell ?: break

                        timelogCells.add(randomFreeCell)
                        timelogPage.addTimeEntryToCell(randomFreeCell, ADD_HOURS)

                        addSum += ADD_HOURS
                    }

                    timelogPage.sleep()
                    Assert.assertEquals((totalSum + addSum), timelogPage.getTotalSum(), DELTA, "New sum is incorrect")

                }
                .nextStep("Delete time entries") {
                    for (timelogCell in timelogCells)
                        timelogPage.deleteTimeEntryFromCell(timelogCell)

                    timelogPage.sleep()

                    Assert.assertEquals(totalSum, timelogPage.getTotalSum(), DELTA, "Old sum is incorrect")
                }

    }

    companion object {

        val DELTA = 0.01
        val ADD_HOURS = 1.25f
    }


}
