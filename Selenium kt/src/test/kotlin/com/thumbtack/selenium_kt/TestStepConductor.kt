package com.thumbtack.selenium_kt

import org.apache.commons.io.FileUtils
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver
import org.openqa.selenium.remote.Augmenter
import java.io.File
import java.io.IOException
import java.util.*

/**
 * Created by pasencukviktor on 15/12/2016
 */
class TestStepConductor(private val webDriver: WebDriver, private val testName: String) {
    private val stepNames = LinkedList<String>()
    private val stepScreenshots = LinkedList<File>()
    private var defaultFolder = "error_screenshots/"

    fun setDefaultFolder(folder: String): TestStepConductor {
        defaultFolder = folder
        if (!defaultFolder.endsWith("/"))
            defaultFolder += "/"
        return this
    }

    @Throws(Throwable::class)
    fun nextStep(stepName: String, step: () -> Unit): TestStepConductor {
        try {
            stepNames.add(stepName)
            step()
            makeScreenshot()
        } catch (throwable: Throwable) {
            makeScreenshot()
            saveScreenshots()
            throw throwable
        }

        return this
    }

    private fun makeScreenshot() {
        val augmentedDriver = Augmenter().augment(webDriver)
        stepScreenshots.add((augmentedDriver as TakesScreenshot).getScreenshotAs(OutputType.FILE))
    }

    private fun saveScreenshots() {
        for (i in stepScreenshots.indices) {
            val file = stepScreenshots[i]
            try {
                FileUtils.copyFile(file, File(String.format("%s%s/%s/Step %d: %s.png", defaultFolder, Date(), testName, i + 1, stepNames[i])))
            } catch (e1: IOException) {
                e1.printStackTrace()
            }

        }
    }
}
