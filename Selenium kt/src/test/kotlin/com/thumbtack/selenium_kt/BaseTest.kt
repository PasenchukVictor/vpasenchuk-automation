package com.thumbtack.selenium_kt

import com.thumbtack.selenium_kt.redmine.LoginPage
import com.thumbtack.selenium_kt.redmine.RedminePage
import io.github.bonigarcia.wdm.ChromeDriverManager
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.testng.Assert
import org.testng.annotations.AfterClass
import org.testng.annotations.BeforeClass

import java.io.IOException

/**
 * Created by pasencukviktor on 15/12/2016
 */
abstract class BaseTest {
    internal lateinit var driver: WebDriver
    private lateinit var siteCredentials: SiteCredentials


    @BeforeClass
    @Throws(IOException::class)
    open fun setupTest() {

        //latest version of Chrome driver
        ChromeDriverManager.getInstance().setup()

        // To run with specific version, setup use following line of code:
        // ChromeDriverManager.getInstance().setup("2.29")

        siteCredentials = SiteCredentials.getInstanceFromFile(configFile)
        driver = ChromeDriver()

        driver.get(siteCredentials.baseUrl)

        val redminePage = RedminePage(driver)

        Assert.assertFalse(redminePage.isLoggedIn, "user is logged in")

        performLogin(redminePage)

        Assert.assertTrue(redminePage.isLoggedIn, "user is not logged in")
    }

    internal abstract val configFile: String

    @AfterClass
    fun teardown() {
        RedminePage(driver).clickSignOutButton()
        driver.quit()
    }


    internal fun performLogin(redminePage: RedminePage) {
        if (!redminePage.isLoggedIn) {
            redminePage.clickSignInButton()

            val loginPage = LoginPage(redminePage.driver)

            loginPage.setUsername(siteCredentials.login)
            loginPage.setPassword(siteCredentials.password)
            loginPage.clickLoginButton()
        }
    }

    companion object {

        val ERROR_SCREENSHOTS_FOLDER = "error_screenshots"
    }
}
