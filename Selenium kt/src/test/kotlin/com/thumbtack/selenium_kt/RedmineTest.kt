package com.thumbtack.selenium_kt

import com.thumbtack.selenium_kt.redmine.ProfilePage
import com.thumbtack.selenium_kt.redmine.RedminePage
import com.thumbtack.selenium_kt.redmine.models.NotificationType
import org.testng.Assert
import org.testng.annotations.Test


/**
 * Created by pasencukviktor on 03/12/2016
 */
class RedmineTest : BaseTest() {

    override val configFile: String
        get() = "toy_redmine.json"

    @Test
    @Throws(Throwable::class)
    fun changeNotificationTypeTest() {
        val redminePage = RedminePage(driver)

        TestStepConductor(driver, "changeNotificationTypeTest")
                .setDefaultFolder(BaseTest.ERROR_SCREENSHOTS_FOLDER)
                .nextStep("Open Notifications") { redminePage.clickProfileButton() }
                .nextStep("Change Profile Settings") {
                    val profilePage = ProfilePage(redminePage)

                    profilePage.selectNotificationType(EXPECTED_NOTIFICATION_TYPE)
                }
                .nextStep("Save Notifications Settings") {
                    val profilePage = ProfilePage(redminePage)
                    profilePage.clickSaveButton()
                }
                .nextStep("Return home") { redminePage.clickHomeButton() }
                .nextStep("Open Profile") { redminePage.clickProfileButton() }
                .nextStep("Check Notifications Settings") {
                    val profilePage = ProfilePage(redminePage)
                    Assert.assertEquals(EXPECTED_NOTIFICATION_TYPE, profilePage.selectedNotificationType, "incorrect notification type")
                }
                .nextStep("Return home") { redminePage.clickHomeButton() }

    }

    @Test
    @Throws(Throwable::class)
    fun changeSelfNotificationsTest() {
        val redminePage = RedminePage(driver)

        TestStepConductor(driver, "changeSelfNotificationsTest")
                .setDefaultFolder(BaseTest.ERROR_SCREENSHOTS_FOLDER)
                .nextStep("Open Profile") { redminePage.clickProfileButton() }
                .nextStep("Change Self Notifications Settings") {
                    val profilePage = ProfilePage(redminePage)

                    profilePage.selectNotificationType(EXPECTED_NOTIFICATION_TYPE)
                    profilePage.setNoSelfNotifiedCheckboxState(true)
                }
                .nextStep("Save Self Notificatiаons Settings") {
                    val profilePage = ProfilePage(redminePage)
                    profilePage.clickSaveButton()
                }
                .nextStep("Return home") { redminePage.clickHomeButton() }
                .nextStep("Open Profile") { redminePage.clickProfileButton() }
                .nextStep("Check Self Notifications Settings") {
                    val profilePage = ProfilePage(redminePage)
                    Assert.assertEquals(EXPECTED_NOTIFICATION_TYPE, profilePage.selectedNotificationType, "incorrect notification type")
                    Assert.assertTrue(profilePage.isNoSelfNotificationsSelected, "self notifications checkbox state is not checked")
                }
                .nextStep("Return home") { redminePage.clickHomeButton() }

    }

    companion object {

        val EXPECTED_NOTIFICATION_TYPE = NotificationType.only_assigned
    }

}
