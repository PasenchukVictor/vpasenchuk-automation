package com.thumbtack.selenium;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by pasencukviktor on 15/12/2016
 */
public class SiteCredentials {

    @SerializedName("BASE_URL")
    private String baseUrl;

    @SerializedName("LOGIN")
    private String login;

    @SerializedName("PASSWORD")
    private String password;

    public SiteCredentials(String baseUrl, String login, String password) {
        this.baseUrl = baseUrl;
        this.login = login;
        this.password = password;
    }

    public static SiteCredentials getInstanceFromFile(String fileName) throws IOException {
        final String s = FileUtils.readFileToString(new File(fileName));
        return new Gson().fromJson(s, SiteCredentials.class);
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
