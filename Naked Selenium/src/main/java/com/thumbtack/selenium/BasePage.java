package com.thumbtack.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by pasencukviktor on 03/12/2016
 */
public abstract class BasePage {
    public static final int DEFAULT_SLEEP_TIME = 5;
    public static final int IMPLICIT_WAIT_TIME = 15;
    private WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        getDriver().manage().timeouts().implicitlyWait(IMPLICIT_WAIT_TIME, TimeUnit.SECONDS);

        PageFactory.initElements(driver, this);
    }

    public BasePage(BasePage page) {
        this(page.getDriver());
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void sleep() {
        sleep(DEFAULT_SLEEP_TIME);
    }

    public void sleep(long seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean isElementPresent(By locator) {
        getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        final boolean b = driver.findElements(locator).size() > 0;
        getDriver().manage().timeouts().implicitlyWait(IMPLICIT_WAIT_TIME, TimeUnit.SECONDS);
        return b;
    }

    public boolean isChildElementPresent(WebElement element, By locator) {
        getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        final boolean b = element.findElements(locator).size() > 0;
        getDriver().manage().timeouts().implicitlyWait(IMPLICIT_WAIT_TIME, TimeUnit.SECONDS);
        return b;
    }

}
