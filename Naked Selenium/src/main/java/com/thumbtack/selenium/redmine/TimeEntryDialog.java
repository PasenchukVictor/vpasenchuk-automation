package com.thumbtack.selenium.redmine;

import com.thumbtack.selenium.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by pasencukviktor on 15/12/2016
 */
public class TimeEntryDialog extends RedminePage {

    @FindBy(id = "new-time-entry-ok-button")
    WebElement okButton;

    @FindBy(id = "delete-time-entry")
    WebElement deleteButton;

    @FindBy(id = "hours")
    WebElement hoursField;

    public TimeEntryDialog(WebDriver webDriver) {
        super(webDriver);

        webDriver.switchTo().activeElement();
    }

    public TimeEntryDialog(BasePage page) {
        super(page);

        switchToactiveElement();
    }

    public void addHours(float hours) {
        hoursField.sendKeys(String.valueOf(hours));
    }


    public void pressOkButton() {
        okButton.click();

        switchToactiveElement();
    }

    public void pressDeleteButton() {
        deleteButton.click();

        switchToactiveElement();
    }

    private void switchToactiveElement() {
        getDriver().switchTo().activeElement();
    }

}
