package com.thumbtack.selenium.redmine;

import com.thumbtack.selenium.BasePage;
import com.thumbtack.selenium.redmine.models.NotificationType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by pasencukviktor on 03/12/2016
 */
public class ProfilePage extends RedminePage {

    @FindBy(xpath = "//input[@id='no_self_notified']")
    private WebElement noSelfNotifiedCheckbox;

    @FindBy(xpath = "//select[@id='user_mail_notification']")
    private WebElement mailNotificationDropdown;

    @FindBy(xpath = "//div[@class='splitcontentleft']/input")
    private WebElement saveButton;


    public ProfilePage(WebDriver webDriver) {
        super(webDriver);
    }

    public ProfilePage(BasePage page) {
        super(page);
    }

    public void selectNotificationType(NotificationType notificationType) {
        new Select(mailNotificationDropdown)
                .selectByValue(notificationType.name());
    }

    public NotificationType getSelectedNotificationType() {
        return NotificationType.valueOf(new Select(mailNotificationDropdown)
                .getFirstSelectedOption().getAttribute("value"));
    }

    public void setNoSelfNotifiedCheckboxState(boolean checked) {
        final boolean noSelfNotificationsSelected = isNoSelfNotificationsSelected();
        if ((noSelfNotificationsSelected && !checked) || (!noSelfNotificationsSelected && checked))
            noSelfNotifiedCheckbox.click();
    }

    public boolean isNoSelfNotificationsSelected() {
        return noSelfNotifiedCheckbox.isSelected();
    }

    public void clickSaveButton() {
        saveButton.click();
    }

}
