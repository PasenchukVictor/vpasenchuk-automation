package com.thumbtack.selenium.redmine;

import com.thumbtack.selenium.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by pasencukviktor on 03/12/2016
 */
public class RedminePage extends BasePage {

    private static final By
            LOGIN_LOCATOR = By.xpath("//li[1]/a[@class='login']"),
            LOGOUT_LOCATOR = By.xpath("//li[2]/a[@class='logout']"),
            TIMELOG_LOCATOR = By.xpath("//li[4]"),
            PROFILE_LOCATOR = By.xpath("//li[1]/a[@class='my-account']");

    @FindBy(xpath = "//a[@class='home']")
    private WebElement homeButton;

    public RedminePage(WebDriver webDriver) {
        super(webDriver);
    }

    public RedminePage(BasePage page) {
        super(page);
    }

    public boolean isLoggedIn() {
        return !isElementPresent(LOGIN_LOCATOR);
    }

    public void clickSignInButton() {
        getDriver().findElement(LOGIN_LOCATOR).click();
    }

    public void clickSignOutButton() {
        getDriver().findElement(LOGOUT_LOCATOR).click();
    }

    public ProfilePage clickProfileButton() {
        getDriver().findElement(PROFILE_LOCATOR).click();
        return new ProfilePage(getDriver());
    }

    public TimelogPage clickTimelogButton() {
        getDriver().findElement(TIMELOG_LOCATOR).click();

        return new TimelogPage(this);
    }

    public void clickHomeButton() {
        homeButton.click();
    }

}
