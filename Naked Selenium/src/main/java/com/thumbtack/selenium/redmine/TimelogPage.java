package com.thumbtack.selenium.redmine;

import com.thumbtack.selenium.redmine.models.TimelogCell;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by pasencukviktor on 14/12/2016
 */
public class TimelogPage extends RedminePage {


    public static final String[] DAILY_SUM_IDS = new String[]{
            "Mon-sum",
            "Tue-sum",
            "Wed-sum",
            "Thu-sum",
            "Fri-sum",
            "Sat-sum",
            "Sun-sum",
    };

    public static final String
            ADD_ENTRY_XPATH = "//tbody/tr[@id='%s']/td[@class='%s']/div[@class='new-time-entry icon-plus']",
            ENTRY_XPATH = "//tbody/tr[@id='%s']/td[@class='%s']";
    public static final By ADD_TIME_ENTRY_LOCATOR = By.xpath(".//div[@class='new-time-entry icon-plus']");


    private final Random random = new Random();

    @FindBy(xpath = "//table[@class='quicktrack-table']/tbody")
    private WebElement timelogTable;

    @FindBy(xpath = "//tbody/tr[@class='summary-row']/td[@class='day-summary-hours summary-hours-total']")
    private WebElement totalSum;

    private LinkedList<TimelogCell> freeTimelogCells = new LinkedList<>();
    private HashSet<String> issues = new HashSet<>(), days = new HashSet<>();

    public TimelogPage(WebDriver webDriver) {
        super(webDriver);
        init();
    }

    public TimelogPage(RedminePage redminePage) {
        super(redminePage);
        init();
    }

    private void init() {

        sleep();

        final List<WebElement> rows = timelogTable.findElements(By.xpath(".//tr[contains (@id, 'project-')]"));

//        freeTimelogCells = rows.stream().flatMap(row -> {
//            final String rowId = row.getAttribute("id");
//            issues.add(rowId);
//            return row.findElements(By.xpath(".//td[contains (@class, 'Days')]"))
//                    .stream()
//                    .filter(column -> isChildElementPresent(column, ADD_TIME_ENTRY_LOCATOR))
//                    .map(column -> new TimelogCell(rowId, column.getAttribute("class")));
//        })
//                .collect(Collectors.toCollection(LinkedList::new));

        for (WebElement row : rows) {
            final String rowId = row.getAttribute("id");
            issues.add(rowId);
            final List<WebElement> columns = row.findElements(By.xpath(".//td[contains (@class, 'Days')]"));
            for (WebElement column : columns) {
                final String columnId = column.getAttribute("class");
                days.add(columnId);

                if (isElementPresent(ADD_TIME_ENTRY_LOCATOR))
                    freeTimelogCells.add(new TimelogCell(rowId, columnId));
            }
        }

    }

    public float getTotalSum() {
        return Float.parseFloat(totalSum.getText());
    }

    public float getTotalSumByDays() {
        float sum = 0;
        for (String dailySumId : DAILY_SUM_IDS) {
            final String dailySum = getDriver()
                    .findElement(By.id(dailySumId))
                    .getText();
            sum += Float.parseFloat(dailySum);
        }
        return sum;
    }

    public float getTotalSumByIssues() {
        float sum = 0;
        for (String issueId : issues) {
            final String issueSum = getDriver()
                    .findElement(By.id(issueId))
                    .findElement(By.className("task-hours-sum"))
                    .getText();
            sum += Float.parseFloat(issueSum);
        }
        return sum;
    }

    public TimelogCell getRandomFreeCell() {
        while (freeTimelogCells.size() > 0) {
            final int i = random.nextInt(freeTimelogCells.size());

            final TimelogCell timelogCell = freeTimelogCells.remove(i);

            if (isElementPresent(getTimelogCellButtonXpath(timelogCell)))
                return timelogCell;
        }
        return null;
    }

    private By getTimelogCellButtonXpath(TimelogCell timelogCell) {
        return By.xpath(String.format(ADD_ENTRY_XPATH, timelogCell.row, timelogCell.column));
    }

    private By getTimelogCellEntryXpath(TimelogCell timelogCell) {
        return By.xpath(String.format(ENTRY_XPATH, timelogCell.row, timelogCell.column));
    }

    public boolean addTimeEntryToCell(TimelogCell timelogCell, float hours) {

        final By timelogCellButtonXpath = getTimelogCellButtonXpath(timelogCell);
        if (isElementPresent(timelogCellButtonXpath)) {
            getDriver()
                    .findElement(timelogCellButtonXpath)
                    .click();

            final TimeEntryDialog timeEntryDialog = new TimeEntryDialog(this);
            timeEntryDialog.addHours(hours);
            timeEntryDialog.pressOkButton();

            return true;

        }

        return false;
    }

    public void deleteTimeEntryFromCell(TimelogCell timelogCell) {

        final By timelogCellButtonXpath = getTimelogCellEntryXpath(timelogCell);
        getDriver()
                .findElement(timelogCellButtonXpath)
                .click();

        final TimeEntryDialog timeEntryDialog = new TimeEntryDialog(this);

        try {
            timeEntryDialog.pressDeleteButton();
        } catch (UnhandledAlertException f) {
            try {
                Alert alert = getDriver().switchTo().alert();
                alert.accept();
            } catch (NoAlertPresentException e) {
                e.printStackTrace();
            }
        }
    }

}
