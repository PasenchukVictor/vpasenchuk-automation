package com.thumbtack.selenium.redmine.models;

/**
 * Created by pasencukviktor on 14/12/2016
 */
public class TimelogCell {

    public final String row, column;

    public TimelogCell(String row, String column) {
        this.row = row;
        this.column = column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TimelogCell that = (TimelogCell) o;

        if (row != null ? !row.equals(that.row) : that.row != null) return false;
        return column != null ? column.equals(that.column) : that.column == null;

    }

    @Override
    public int hashCode() {
        int result = row != null ? row.hashCode() : 0;
        result = 31 * result + (column != null ? column.hashCode() : 0);
        return result;
    }
}
