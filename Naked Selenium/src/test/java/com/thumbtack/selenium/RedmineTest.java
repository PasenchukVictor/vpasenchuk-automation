package com.thumbtack.selenium;

import com.thumbtack.selenium.redmine.ProfilePage;
import com.thumbtack.selenium.redmine.RedminePage;
import com.thumbtack.selenium.redmine.models.NotificationType;
import org.testng.Assert;
import org.testng.annotations.Test;


/**
 * Created by pasencukviktor on 03/12/2016
 */
public class RedmineTest extends BaseTest {

    public static final NotificationType EXPECTED_NOTIFICATION_TYPE = NotificationType.only_assigned;

    @Override
    String getConfigFile() {
        return "toy_redmine.json";
    }

    @Test
    public void changeNotificationTypeTest() throws Throwable {
        final RedminePage redminePage = new RedminePage(driver);

        new TestStepConductor(driver, "changeNotificationTypeTest")
                .setDefaultFolder(ERROR_SCREENSHOTS_FOLDER)
                .nextStep("Open Notifications", redminePage::clickProfileButton)
                .nextStep("Change Profile Settings", () -> {
                    final ProfilePage profilePage = new ProfilePage(redminePage);

                    profilePage.selectNotificationType(EXPECTED_NOTIFICATION_TYPE);
                })
                .nextStep("Save Notifications Settings", () -> {
                    final ProfilePage profilePage = new ProfilePage(redminePage);
                    profilePage.clickSaveButton();
                })
                .nextStep("Return home", redminePage::clickHomeButton)
                .nextStep("Open Profile", redminePage::clickProfileButton)
                .nextStep("Check Notifications Settings", () -> {
                    final ProfilePage profilePage = new ProfilePage(redminePage);
                    Assert.assertEquals(EXPECTED_NOTIFICATION_TYPE, profilePage.getSelectedNotificationType(), "incorrect notification type");
                })
                .nextStep("Return home", redminePage::clickHomeButton);

    }

    @Test
    public void changeSelfNotificationsTest() throws Throwable {
        final RedminePage redminePage = new RedminePage(driver);

        new TestStepConductor(driver, "changeSelfNotificationsTest")
                .setDefaultFolder(ERROR_SCREENSHOTS_FOLDER)
                .nextStep("Open Profile", redminePage::clickProfileButton)
                .nextStep("Change Self Notifications Settings", () -> {
                    final ProfilePage profilePage = new ProfilePage(redminePage);

                    profilePage.selectNotificationType(EXPECTED_NOTIFICATION_TYPE);
                    profilePage.setNoSelfNotifiedCheckboxState(true);
                })
                .nextStep("Save Self Notifications Settings", () -> {
                    final ProfilePage profilePage = new ProfilePage(redminePage);
                    profilePage.clickSaveButton();
                })
                .nextStep("Return home", redminePage::clickHomeButton)
                .nextStep("Open Profile", redminePage::clickProfileButton)
                .nextStep("Check Self Notifications Settings", () -> {
                    final ProfilePage profilePage = new ProfilePage(redminePage);
                    Assert.assertEquals(EXPECTED_NOTIFICATION_TYPE, profilePage.getSelectedNotificationType(), "incorrect notification type");
                    Assert.assertTrue(profilePage.isNoSelfNotificationsSelected(), "self notifications checkbox state is not checked");
                })
                .nextStep("Return home", redminePage::clickHomeButton);

    }

}
