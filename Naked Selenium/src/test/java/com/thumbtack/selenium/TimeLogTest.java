package com.thumbtack.selenium;

import com.thumbtack.selenium.redmine.RedminePage;
import com.thumbtack.selenium.redmine.TimelogPage;
import com.thumbtack.selenium.redmine.models.TimelogCell;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.LinkedList;

/**
 * Created by pasencukviktor on 15/12/2016
 */
public class TimeLogTest extends BaseTest {

    public static final double DELTA = 0.01;
    public static final float ADD_HOURS = 1.25f;

    private TimelogPage timelogPage;


    @Override
    String getConfigFile() {
        return "real_redmine.json";
    }

    @Override
    @BeforeClass
    public void setupTest() throws IOException {
        super.setupTest();

        timelogPage = new RedminePage(driver).clickTimelogButton();
    }

    @Test
    public void timeLogTestRowSum() throws Throwable {

        new TestStepConductor(driver, "timeLogTestRowSum")
                .setDefaultFolder(ERROR_SCREENSHOTS_FOLDER)
                .nextStep("Check row values", () -> Assert.assertEquals(timelogPage.getTotalSum(), timelogPage.getTotalSumByIssues(), DELTA, "Row sum is incorrect"));

    }

    @Test
    public void timeLogTestColumnSum() throws Throwable {
        new TestStepConductor(driver, "timeLogTestColumnSum")
                .setDefaultFolder(ERROR_SCREENSHOTS_FOLDER)
                .nextStep("Check column values", () -> Assert.assertEquals(timelogPage.getTotalSum(), timelogPage.getTotalSumByDays(), DELTA, "Column sum is incorrect"));

    }

    @Test
    public void timeLogTestAddTime() throws Throwable {
        final LinkedList<TimelogCell> timelogCells = new LinkedList<>();

        final float[] totalSum = new float[1];
        new TestStepConductor(driver, "timeLogTestColumnSum")
                .setDefaultFolder(ERROR_SCREENSHOTS_FOLDER)
                .nextStep("Add time entries", () -> {

                    totalSum[0] = timelogPage.getTotalSum();

                    float addSum = 0;
                    for (int i = 0; i < 3; i++) {
                        final TimelogCell randomFreeCell = timelogPage.getRandomFreeCell();

                        if (randomFreeCell == null)
                            break;

                        timelogCells.add(randomFreeCell);
                        timelogPage.addTimeEntryToCell(randomFreeCell, ADD_HOURS);

                        addSum += ADD_HOURS;
                    }

                    timelogPage.sleep();
                    Assert.assertEquals(totalSum[0] + addSum, timelogPage.getTotalSum(), DELTA, "New sum is incorrect");

                })
                .nextStep("Delete time entries", () -> {
                    for (TimelogCell timelogCell : timelogCells)
                        timelogPage.deleteTimeEntryFromCell(timelogCell);

                    timelogPage.sleep();

                    Assert.assertEquals(totalSum[0], timelogPage.getTotalSum(), DELTA, "Old sum is incorrect");
                });

    }


}
