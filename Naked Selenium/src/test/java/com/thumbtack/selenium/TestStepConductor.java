package com.thumbtack.selenium;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;

/**
 * Created by pasencukviktor on 15/12/2016
 */
public class TestStepConductor {

    private String testName;
    private WebDriver webDriver;
    private LinkedList<String> stepNames = new LinkedList<>();
    private LinkedList<File> stepScreenshots = new LinkedList<>();
    private String defaultFolder = "error_screenshots/";

    public TestStepConductor(WebDriver webDriver, String testName) {
        this.webDriver = webDriver;
        this.testName = testName;
    }

    public TestStepConductor setDefaultFolder(String folder) {
        defaultFolder = folder;
        if (!defaultFolder.endsWith("/"))
            defaultFolder += "/";
        return this;
    }

    public TestStepConductor nextStep(String stepName, Runnable step) throws Throwable {
        try {
            stepNames.add(stepName);
            step.run();
            makeScreenshot();
        } catch (Throwable throwable) {
            makeScreenshot();
            saveScreenshots();
            throw throwable;
        }
        return this;
    }

    private void makeScreenshot() {
        final WebDriver augmentedDriver = new Augmenter().augment(webDriver);
        stepScreenshots.add(((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE));
    }

    private void saveScreenshots() {
        for (int i = 0; i < stepScreenshots.size(); i++) {
            File file = stepScreenshots.get(i);
            try {
                FileUtils.copyFile(file, new File(String.format("%s%s/%s/Step %d: %s.png", defaultFolder, new Date(), testName, (i + 1), stepNames.get(i))));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
