package com.thumbtack.selenium;

import com.thumbtack.selenium.redmine.LoginPage;
import com.thumbtack.selenium.redmine.RedminePage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

/**
 * Created by pasencukviktor on 15/12/2016
 */
public abstract class BaseTest {

    public static final String ERROR_SCREENSHOTS_FOLDER = "error_screenshots";
    WebDriver driver;
    private SiteCredentials siteCredentials;


    @BeforeClass
    public void setupTest() throws IOException {

        //latest version of Chrome driver
        ChromeDriverManager.getInstance().setup();
        // To run with specific version, setup use following line of code:

        // ChromeDriverManager.getInstance().setup("2.29");
        siteCredentials = SiteCredentials.getInstanceFromFile(getConfigFile());
        driver = new ChromeDriver();

        driver.get(siteCredentials.getBaseUrl());

        final RedminePage redminePage = new RedminePage(driver);

        Assert.assertFalse(redminePage.isLoggedIn(), "user is logged in");

        performLogin(redminePage);

        Assert.assertTrue(redminePage.isLoggedIn(), "user is not logged in");
    }

    abstract String getConfigFile();

    @AfterClass
    public void teardown() {
        new RedminePage(driver).clickSignOutButton();
        if (driver != null) {
            driver.quit();
        }
    }


    void performLogin(RedminePage redminePage) {
        if (!redminePage.isLoggedIn()) {
            redminePage.clickSignInButton();

            final LoginPage loginPage = new LoginPage(redminePage.getDriver());

            loginPage.setUsername(siteCredentials.getLogin());
            loginPage.setPassword(siteCredentials.getPassword());
            loginPage.clickLoginButton();
        }
    }
}
