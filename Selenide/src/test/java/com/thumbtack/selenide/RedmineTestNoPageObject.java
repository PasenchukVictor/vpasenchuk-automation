package com.thumbtack.selenide;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.thumbtack.selenide.redmine.NotificationType;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by pasencukviktor on 03/12/2016
 */
public class RedmineTestNoPageObject {

    public static final String REDMINE_MAIN_PAGE_URL = "http://redmine-train.dev.thumbtack.net:3000/";
    public static final NotificationType NOTIFICATION_TYPE = NotificationType.only_assigned;

    public static final By
            LOGIN_BUTTON_LOCATOR = By.xpath("//li[1]/a[@class='login']"),
            LOGOUT_BUTTON_LOCATOR = By.xpath("//li[2]/a[@class='logout']"),
            USERNAME_FIELD_LOCATOR = By.xpath("//input[@id='username']"),
            PASSWORD_FIELD_LOCATOR = By.xpath("//input[@id='password']"),
            SUBMIT_LOGIN_BUTTON_LOCATOR = By.xpath("//tr[4]/td[2]/input"),
            PROFILE_BUTTON_LOCATOR = By.xpath("//li[1]/a[@class='my-account']"),
            HOME_BUTTON_LOCATOR = By.xpath("//a[@class='home']"),
            SAVE_PROFILE_BUTTON_LOCATOR = By.xpath("//div[@class='splitcontentleft']/input"),
            NOTIFICATION_TYPE_DROPDOWN_LOCATOR = By.xpath("//select[@id='user_mail_notification']"),
            NO_SELF_NOTIFY_CHECKBOX_LOCATOR = By.xpath("//input[@id='no_self_notified']");

    private WebDriver driver;

    @BeforeClass
    public static void setupClass() {
    }

    @Before
    public void setupTest() {
        Configuration.browser = "chrome";
        ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();
        WebDriverRunner.setWebDriver(driver);
    }

    @After
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void loginTest() {

        performLogin();

        performLogout();
    }


    @Test
    public void changeSettingsTest() {
        // Your test code here
        performLogin();

        elementShouldAppear(PROFILE_BUTTON_LOCATOR);

        clickElement(PROFILE_BUTTON_LOCATOR);

        elementShouldAppear(NOTIFICATION_TYPE_DROPDOWN_LOCATOR);


        Selenide.$(NOTIFICATION_TYPE_DROPDOWN_LOCATOR).val(NOTIFICATION_TYPE.name());

        Selenide.$(NO_SELF_NOTIFY_CHECKBOX_LOCATOR).setSelected(true);

        clickElement(SAVE_PROFILE_BUTTON_LOCATOR);

        Selenide.sleep(2000);

        clickElement(HOME_BUTTON_LOCATOR);

        Selenide.sleep(2000);

        clickElement(PROFILE_BUTTON_LOCATOR);

        elementShouldAppear(NOTIFICATION_TYPE_DROPDOWN_LOCATOR);

        Selenide.$(NOTIFICATION_TYPE_DROPDOWN_LOCATOR).is(Condition.value(NOTIFICATION_TYPE.name()));
        Selenide.$(NO_SELF_NOTIFY_CHECKBOX_LOCATOR).is(Condition.selected);

        performLogout();
    }

    private void performLogin() {
        Selenide.open(REDMINE_MAIN_PAGE_URL);

        elementShouldAppear(LOGIN_BUTTON_LOCATOR);

        clickElement(LOGIN_BUTTON_LOCATOR);

        elementShouldAppear(USERNAME_FIELD_LOCATOR);


        Selenide.$(USERNAME_FIELD_LOCATOR).sendKeys("vpasenchuk");
        Selenide.$(PASSWORD_FIELD_LOCATOR).sendKeys("SxXyJ9fX");

        clickElement(SUBMIT_LOGIN_BUTTON_LOCATOR);
    }

    private void performLogout() {
        elementShouldAppear(LOGOUT_BUTTON_LOCATOR);
        clickElement(LOGOUT_BUTTON_LOCATOR);

        elementShouldAppear(LOGIN_BUTTON_LOCATOR);
    }

    private void clickElement(By profileButtonLocator) {
        Selenide.$(profileButtonLocator).click();
    }

    private void elementShouldAppear(By profileButtonLocator) {
        Selenide.$(profileButtonLocator).should(Condition.appear);
    }

}
