package com.thumbtack.selenide;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.thumbtack.selenide.redmine.LoginPage;
import com.thumbtack.selenide.redmine.MainPage;
import com.thumbtack.selenide.redmine.NotificationType;
import com.thumbtack.selenide.redmine.ProfilePage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by pasencukviktor on 03/12/2016
 */
public class RedmineTestPageObject {

    public static final String REDMINE_MAIN_PAGE_URL = "http://redmine-train.dev.thumbtack.net:3000/";
    public static final NotificationType NOTIFICATION_TYPE = NotificationType.only_assigned;
    private WebDriver driver;

    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup();
    }

    @Before
    public void setupTest() {
        Configuration.browser = "chrome";
        ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();
        WebDriverRunner.setWebDriver(driver);
    }

    @After
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void loginTest() throws Throwable {
        // Your test code here
        final MainPage mainPage = Selenide.open(REDMINE_MAIN_PAGE_URL, MainPage.class);

        performLogin(mainPage);

        performLogout(mainPage);

    }


    @Test
    public void changeSettingsTest() throws Throwable {
        // Your test code here
        final MainPage mainPage = Selenide.open(REDMINE_MAIN_PAGE_URL, MainPage.class);

        performLogin(mainPage);

        final ProfilePage profilePage = setupProfileSettings(mainPage);

        Assert.assertTrue("incorrect notification type", NOTIFICATION_TYPE.equals(profilePage.getSelectedNotificationType()));
        Assert.assertTrue("incorrect self notifications checkbox state", profilePage.isNoSelfNotificationsSelected());

        performLogout(mainPage);

        Assert.assertTrue("user is logged in", !mainPage.isLoggedIn());
    }

    private void performLogin(MainPage mainPage) {

        mainPage.waitForLoginButtonLoading();

        mainPage.clickSignInButton();

        final LoginPage loginPage = mainPage.clickSignInButton();
        ;

        loginPage.setUsername("vpasenchuk");
        loginPage.setPassword("SxXyJ9fX");

        loginPage.clickLoginButton();

        mainPage.waitForLogoutButtonLoading();

    }

    private void performLogout(MainPage mainPage) {
        mainPage.clickSignOutButton();
        mainPage.waitForLoginButtonLoading();
    }

    private ProfilePage setupProfileSettings(MainPage mainPage) {

        final ProfilePage profilePage = mainPage.clickProfileButton();

        setupFields(profilePage);

        returnToHomeAndGoBack(profilePage);

        return profilePage;
    }

    private void setupFields(ProfilePage profilePage) {
        profilePage.selectNotificationType(NOTIFICATION_TYPE);
        profilePage.setNoSelfNotifiedCheckboxState(true);
        profilePage.clickSaveButton();
    }

    private void returnToHomeAndGoBack(ProfilePage profilePage) {
        profilePage.clickHomeButton();
        profilePage.clickProfileButton();
    }


}
