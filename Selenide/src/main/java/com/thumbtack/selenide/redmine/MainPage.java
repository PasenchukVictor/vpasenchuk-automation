package com.thumbtack.selenide.redmine;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

/**
 * Created by pasencukviktor on 03/12/2016
 */
public class MainPage {

    private static final By LOGIN_LOCATOR = By.xpath("//li[1]/a[@class='login']");
    private static final By LOGOUT_LOCATOR = By.xpath("//li[2]/a[@class='logout']");
    private static final By PROFILE_LOCATOR = By.xpath("//li[1]/a[@class='my-account']");

    @FindBy(xpath = "//a[@class='home']")
    private SelenideElement homeButton;


    public boolean isLoggedIn() {
        return Selenide.$(LOGOUT_LOCATOR).isDisplayed();
    }

    public LoginPage clickSignInButton() {
        Selenide.$(LOGIN_LOCATOR).click();
        return Selenide.page(LoginPage.class);
    }

    public void clickSignOutButton() {
        Selenide.$(LOGOUT_LOCATOR).click();
    }

    public ProfilePage clickProfileButton() {
        Selenide.$(PROFILE_LOCATOR).click();
        return Selenide.page(ProfilePage.class);
    }

    public void clickHomeButton() {
        homeButton.click();
    }

    public void waitForLoginButtonLoading() {
        Selenide.$(LOGIN_LOCATOR).shouldBe(Condition.appear);
    }

    public void waitForLogoutButtonLoading() {
        Selenide.$(LOGOUT_LOCATOR).shouldBe(Condition.appear);
    }
}
