package com.thumbtack.selenide.redmine;

/**
 * Created by pasencukviktor on 03/12/2016
 */
public enum NotificationType {
    all,
    only_my_events,
    only_assigned,
    only_owner,
    none,
}
