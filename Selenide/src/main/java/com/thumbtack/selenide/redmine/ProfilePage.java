package com.thumbtack.selenide.redmine;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by pasencukviktor on 03/12/2016
 */
public class ProfilePage extends MainPage {

    @FindBy(xpath = "//input[@id='no_self_notified']")
    private SelenideElement noSelfNotifiedCheckbox;

    @FindBy(xpath = "//select[@id='user_mail_notification']")
    private SelenideElement mailNotificationDropdown;

    @FindBy(xpath = "//div[@class='splitcontentleft']/input")
    private SelenideElement saveButton;


    public void selectNotificationType(NotificationType notificationType) {
        mailNotificationDropdown.selectOptionByValue(notificationType.name());
    }

    public NotificationType getSelectedNotificationType() {
        return NotificationType.valueOf(mailNotificationDropdown.getSelectedValue());
    }

    public void setNoSelfNotifiedCheckboxState(boolean checked) {
        final boolean noSelfNotificationsSelected = isNoSelfNotificationsSelected();
        if ((noSelfNotificationsSelected && !checked) || (!noSelfNotificationsSelected && checked))
            noSelfNotifiedCheckbox.click();
    }

    public boolean isNoSelfNotificationsSelected() {
        return noSelfNotifiedCheckbox.isSelected();
    }

    public void clickSaveButton() {
        saveButton.click();
    }


}
